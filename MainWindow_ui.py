# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainDialogUI(object):
    def setupUi(self, MainDialogUI):
        MainDialogUI.setObjectName(_fromUtf8("MainDialogUI"))
        MainDialogUI.resize(347, 119)
        self.verticalLayout = QtGui.QVBoxLayout(MainDialogUI)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(MainDialogUI)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.comboBox = QtGui.QComboBox(MainDialogUI)
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.horizontalLayout.addWidget(self.comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(MainDialogUI)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.FileNameEdit = QtGui.QLineEdit(MainDialogUI)
        self.FileNameEdit.setObjectName(_fromUtf8("FileNameEdit"))
        self.horizontalLayout_2.addWidget(self.FileNameEdit)
        self.OpenFileButton = QtGui.QPushButton(MainDialogUI)
        self.OpenFileButton.setObjectName(_fromUtf8("OpenFileButton"))
        self.horizontalLayout_2.addWidget(self.OpenFileButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_3 = QtGui.QLabel(MainDialogUI)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_3.addWidget(self.label_3)
        self.WebLinkEdit = QtGui.QLineEdit(MainDialogUI)
        self.WebLinkEdit.setObjectName(_fromUtf8("WebLinkEdit"))
        self.horizontalLayout_3.addWidget(self.WebLinkEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(MainDialogUI)
        QtCore.QMetaObject.connectSlotsByName(MainDialogUI)

    def retranslateUi(self, MainDialogUI):
        MainDialogUI.setWindowTitle(_translate("MainDialogUI", "Dialog", None))
        self.label.setText(_translate("MainDialogUI", "Host Name", None))
        self.label_2.setText(_translate("MainDialogUI", "File Name", None))
        self.OpenFileButton.setText(_translate("MainDialogUI", "open", None))
        self.label_3.setText(_translate("MainDialogUI", "Web Link", None))

