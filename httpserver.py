#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import stun
from socket import *
from PyQt4.QtGui import *
from PyQt4.QtCore import *
import sys
from MainWindow_ui import *
import shutil
import os
import hashlib

PORT_NUMBER = 8080
#####################################################################
def CreateWebLink(IP,FileName,hash):
        ResStr = r"http:\\" + IP + ":" + str(PORT_NUMBER) + "/"+ FileName + "?" + hash
        return ResStr

#####################################################################
def CreateHash(FullFileName):
        return hashlib.md5(open(FullFileName, 'rb').read()).hexdigest()        


#####################################################################
#This class will handles any incoming request from
#the browser 
class myHandler(BaseHTTPRequestHandler):
        
        #Handler for the GET requests
        def do_GET(self):
                strpath = str(self.path)
                (filename, hashfile2) = strpath.split('?')
                print filename
                print hashfile2
                filepath = os.path.abspath(os.curdir)+r"/DIR" + filename
                print filepath
                if os.path.exists(filepath):
                        hashfile1 = CreateHash(filepath)
                        print hashfile1
                        if hashfile1 == hashfile2:
                                self.path = filename
                                self.send_response(200)
                                self.send_header("Content-type", "application/octet-stream")
                                self.end_headers()
                                self.wfile.write(open(filepath, "rb").read())
                        else:
                                self.send_response(200)
                                self.send_header('Content-type','text/html')
                                self.end_headers()
                                # Send the html message
                                self.wfile.write("error: file has been changed!")
                                
                else:
                        self.send_response(200)
                        self.send_header('Content-type','text/html')
                        self.end_headers()
                        # Send the html message
                        self.wfile.write("error: file not found!")
                return
        
#####################################################################
#MainDialog
class MainWindow(QDialog, Ui_MainDialogUI):
        def __init__(self,parent):
                # Запускаем родительский конструктор и присоединяем слоты к методам
                # Set up the user interface from Designer.
                QDialog.__init__(self)
                self.setupUi(self)
                #внешний IP
                s = socket(AF_INET, SOCK_DGRAM)
                s.connect(("google.com",80))
                self.comboBox.addItem((s.getsockname()[0]))
                s.close()
                nat_type, external_ip, external_port = stun.get_ip_info()
                self.comboBox.addItem(str(external_ip)+":"+str(external_port))
                self._connectSlots()
        def _connectSlots(self):
                # Устанавливаем обработчики сингналов на кнопки
                self.connect(self.OpenFileButton,SIGNAL("clicked()"),self._slotOpenFile)
        def _slotOpenFile(self):
                # открытие файла
                filename = str(QFileDialog.getOpenFileName(self, 'Open file', ''))
                self.FileNameEdit.setText(filename)
                #папка с файлами
                serverdir = os.path.abspath(os.curdir)+r"/DIR"
                if not (os.path.exists(serverdir)):
                        os.mkdir(serverdir)
                # копируем
                smallfileName = os.path.basename(filename)
                shutil.copy(filename, serverdir+r"/"+smallfileName)
                #создаем web ссылку
                hashfile = CreateHash(serverdir+r"/"+smallfileName)
                IP = str(self.comboBox.currentText())
                weblink = CreateWebLink(IP,smallfileName,hashfile)
                self.WebLinkEdit.setText(weblink)
                        
#####################################################################
def main_client():
        #приложение для создания ссылок
        app = QApplication(sys.argv)
        window = MainWindow(app)
        window.show()
        sys.exit(app.exec_())
        
#####################################################################
def main_server():
        try:
                #Create a web server and define the handler to manage the
                #incoming request
                server = HTTPServer(('', PORT_NUMBER), myHandler)
                print('Started httpserver on port ' , PORT_NUMBER)
                #Wait forever for incoming htto requests
                server.serve_forever()
        except KeyboardInterrupt:
                print('^C received, shutting down the web server')
                server.socket.close()
                
#####################################################################
if __name__ == "__main__":
        app = raw_input("server or client: ")
        if(app == 'c'):
                main_client()
        elif (app == 's'):
                main_server()
        else:
                print "exit"
